import requests
import json
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

import SmarmyBot.CommandParser

# Create your views here
@csrf_exempt
def index(request):
	if request.method == "POST":
		req = json.loads(request.body)
		##print(req["text"])  
		command_response =  SmarmyBot.CommandParser.parseMessage(req)
		requests.post("https://api.groupme.com/v3/bots/post", data=command_response)
		return HttpResponse(status=204)
	else:
		return HttpResponse('<pre>'+request.method +'</pre>')

