import random
config = {
	'chat_ids' : {
		'31536070':{
			'bot_id':'eb78a45e0e6324a5a001b241a0',
			'chat_name':'test enviro'
		'18399957':{
			'bot_id':'4e0987be206e22200c1c8c6bfb',
			'chat_name': 'LEAGUE OF DANK SOULS'
		}
	},
	'response_dict':{
		"rice":["🍚 🍚 🍚 🍚 🍚 🍚 🍚 🍚 🍚","🍚══ლ(́-౪ -`ლ)","Rice is great if you're really hungry and want to eat two thousand of something"],
		"makemeabagel":["(つ◕౪◕)つ━☆ﾟ.*･｡ﾟ  ⊚"],
		"howdeepistoodeep":["I'm afraid I don't undestand the question. By definition, there cannot be a 'too deep'"],
		"me2":["The circumstances you have described also affect me in a similar manner. I express my gratitude."],
		"stand":["ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ \nＴＨＩＳ 　ＭＵＳＴ 　ＢＥ 　ＴＨＥ 　ＷＯＲＫ 　ＯＦ 　ＡＮ 　ＥＮＥＭＹ 「ＳＴＡＮＤ」！！\nゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ"],
		"team":["Error: Team not found. Perhaps you have made a mistake?"],
		"magic8ball":["It is Certain","It is decidedly so","Without a Doubt","Yes, definitely","You may rely on it","As I see it, yes","Most likely","Outlook good","Yes","Signs Point to yes", "Reply Hazy. Try again", "Ask again later", "BEtter not tell you now", "Coannot predict now", "Concentrate and ask again", "Don't count on it", "My reply is no","My sources say no","Outlook is not good","Very doubtful"],
		"thinking":["🤔🤔🤔"],
		"flash":["HE FLAAAASHED?!"],
		"unlucky":["How unfortunate", "Get dunked on son", "Git Gut", "Sucks to Suck", "That wasn't unlucky, that was just you being bad","!team", "Actually, this time you are lucky"],
		"heresy":["The difference between heresy and treachery is ignorance.", "Burn the heretic. Kill the mutant. Purge the unclean.", "Some may question my right to destroy ten billion people. Those who understand realise that I have no right to let them live!", "It is better to die in vain than to live an abomination. The craven and the unready are justly abhorred.", "He who allows the alien to live shares in the crime of its existence.", "This is the Emperor’s planet, the Emperor’s Throne damn you. You want it, you traitor pigs? Come take it from us!", "There is a terrible darkness descending upon the galaxy, and we shall not see it ended in our lifetimes.", "By the Throne, you are a sycophantic weasel! If ever a windpipe cried out for a brisk half-hitch, it yours.", "This place is infected by Chaos' taint.", "There is no such thing as a plea of innocence in my court. A plea of innocence is guilty of wasting my time. Guilty.", "Foul sorcery shall not lay low the servants of the Emperor! The power of the Emperor compels thee and the radiant light of faith abjures thee! Cast thyself before the Emperor's mercy that he might absolve you of your sins, even as he drains you of your very soul!"]
	},
	'CommandTypeTranslation' = {0: 'static', 1: 'quote', 2:'addquote', 3: 'dynamic'},
	'CommandTypeDic' ={
		'makemeabagel':0, 
		'howdeepistoodeep':0,
		'me2':0,
		'stand':0,
		'team':0, 
		'rice':0, 
		'magic8ball':0, 
		'thinking':0, 
		'flash':0,
		'unlucky':0,
		'heresy':0,
		'quote':1}
}

# RESPONSE_DICT = {
	# "rice":["🍚 🍚 🍚 🍚 🍚 🍚 🍚 🍚 🍚","🍚══ლ(́-౪ -`ლ)","Rice is great if you're really hungry and want to eat two thousand of something"],
	# "makemeabagel":["(つ◕౪◕)つ━☆ﾟ.*･｡ﾟ  ⊚"],
	# "howdeepistoodeep":["I'm afraid I don't undestand the question. By definition, there cannot be a 'too deep'"],
	# "me2":["The circumstances you have described also affect me in a similar manner. I express my gratitude."],
	# "stand":["ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ \nＴＨＩＳ 　ＭＵＳＴ 　ＢＥ 　ＴＨＥ 　ＷＯＲＫ 　ＯＦ 　ＡＮ 　ＥＮＥＭＹ 「ＳＴＡＮＤ」！！\nゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ"],
	# "team":["Error: Team not found. Perhaps you have made a mistake?"],
	# "magic8ball":["It is Certain","It is decidedly so","Without a Doubt","Yes, definitely","You may rely on it","As I see it, yes","Most likely","Outlook good","Yes","Signs Point to yes", "Reply Hazy. Try again", "Ask again later", "BEtter not tell you now", "Coannot predict now", "Concentrate and ask again", "Don't count on it", "My reply is no","My sources say no","Outlook is not good","Very doubtful"],
	# "thinking":["🤔🤔🤔"],
	# "flash":["HE FLAAAASHED?!"],
	# "unlucky":["How unfortunate", "Get dunked on son", "Git Gut", "Sucks to Suck", "That wasn't unlucky, that was just you being bad","!team", "Actually, this time you are lucky"],
	# "heresy":["The difference between heresy and treachery is ignorance.", "Burn the heretic. Kill the mutant. Purge the unclean.", "Some may question my right to destroy ten billion people. Those who understand realise that I have no right to let them live!", "It is better to die in vain than to live an abomination. The craven and the unready are justly abhorred.", "He who allows the alien to live shares in the crime of its existence.", "This is the Emperor’s planet, the Emperor’s Throne damn you. You want it, you traitor pigs? Come take it from us!", "There is a terrible darkness descending upon the galaxy, and we shall not see it ended in our lifetimes.", "By the Throne, you are a sycophantic weasel! If ever a windpipe cried out for a brisk half-hitch, it yours.", "This place is infected by Chaos' taint.", "There is no such thing as a plea of innocence in my court. A plea of innocence is guilty of wasting my time. Guilty.", "Foul sorcery shall not lay low the servants of the Emperor! The power of the Emperor compels thee and the radiant light of faith abjures thee! Cast thyself before the Emperor's mercy that he might absolve you of your sins, even as he drains you of your very soul!"]
# }
# CHAT_ID_DICT = {
	# '31536070':{
		# 'bot_id':'eb78a45e0e6324a5a001b241a0',
		# 'chat_name':'test enviro'
	# '18399957':{
		# 'bot_id':'4e0987be206e22200c1c8c6bfb',
		# 'chat_name': 'LEAGUE OF DANK SOULS'
	# }
# }
# def getChatID(message):
	# return CHAT_ID_DICT[message['group_id']]
	
# def getResponse(message, command):
	# #return empty if from an unrecognized chat
	# if (not message['group_id'] in CHAT_ID_DICT):
		# return {}
		
	# if(command in RESPONSE_DICT):
		# res_list = RESPONSE_DICT[command]
		# rand = random.randrange(len(res_list))
		# return formatTextResponse(res_list[rand], getChatID(message))
	# else:
		# return{globals()["response_"+command](message)}

# def formatTextResponse(text, id):
	# return {'bot_id': id, 'text': text}


# def makemeabagel(message):
	# return formatTextResponse('(つ◕౪◕)つ━☆ﾟ.*･｡ﾟ  ⊚', getChatID(message))

# def howdeepistoodeep(message):
	# too_deep = "I'm afraid I don't undestand the question. By definition, there cannot be a 'too deep'"
	# return formatTextResponse(too_deep, getChatID(message))
	# 1
# def me2(message):
	# me_too = "The circumstances you have described also affect me in a similar manner. I express my gratitude."
	# return formatTextResponse(me_too, getChatID(message))

# def rice(message):
	# response1 = "🍚 🍚 🍚"
	# response2 = "🍚══ლ(́-౪ -`ლ)"
	# resopons3 = "Rice is great if you're really hungry and want to eat two thousand of something"

# def stand(message):
	# enemy_stand = "ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ \nＴＨＩＳ 　ＭＵＳＴ 　ＢＥ 　ＴＨＥ 　ＷＯＲＫ 　ＯＦ 　ＡＮ 　ＥＮＥＭＹ 「ＳＴＡＮＤ」！！\nゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ ゴ"
	# return formatTextResponse(enemy_stand, getChatID(message))
	
# def team(message):
	# answer = "Error: Team not found. Perhaps you have made a mistake?"
	# return formatTextResponse(answer, getChatID(message))

# def magic8ball(message):
	# random.seed();
	# rand = random.randrange(0, 19)
	# possible_answers = ["It is Certain","It is decidedly so","Without a Doubt","Yes, definitely","You may rely on it","As I see it, yes","Most likely","Outlook good","Yes","Signs Point to yes", "Reply Hazy. Try again", "Ask again later", "BEtter not tell you now", "Coannot predict now", "Concentrate and ask again", "Don't count on it", "My reply is no","My sources say no","Outlook is not good","Very doubtful"]
	# answer = possible_answers[rand]
	# return formatTextResponse(answer, getChatID(message))
	