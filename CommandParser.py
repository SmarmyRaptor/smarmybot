import random
import SmarmyBot.ConfigFile 

CHAT_ID_DICT = SmarmyBot.ConfigFile.config['chat_ids']
RESPONSE_DICT = SmarmyBot.ConfigFile.config['response_dict']


##dictionary of the differnt command action types
CommandTypeTranslation = SmarmyBot.ConfigFile.config['CommandTypeTranslation']
CommandTypeDic = SmarmyBot.ConfigFile.config['CommandTypeDic']

#returns a int denotiing what kind of command was issued, or -1 if the command doesn't exist
def commandType(text):
	command_name = text[0][1:].lower()
	if(command_name in CommandTypeDic):
		return CommandTypeDic[command_name]
	return -1

	
def parseMessage(message):
	message_text = message['text'].split();
	if message_text[0][0] != '!':
		return {};
	
	command_type = commandType(message_text);
	if command_type != -1:
		if command_type == 0:
			command = getStaticResponse(message, message_text[0][1:].lower())
			return command
		if commandType == 1:
			
	return {}

def getChatID(message):
	return SmarmyBot.ConfigFile.config['chat_ids'][message['group_id']]['bot_id']
	
def getStaticResponse(message, command):

	#return empty if from an unrecognized chat
	if (not message['group_id'] in CHAT_ID_DICT):
		return {}
		
	if(command in RESPONSE_DICT):
		res_list = RESPONSE_DICT[command]
		rand = random.randrange(len(res_list))
		return formatTextResponse(res_list[rand], getChatID(message))
	else:
		return{globals()["response_"+command](message)}

def formatTextResponse(text, id):
	return {'bot_id': id, 'text': text}